# Multithreaded File Server
The Multithreaded File Server is a Go-based file server similar to Python's http.server module. It allows you to serve files from a specified directory and listen on a specific port. This project aims to provide a simple and efficient way to share files over HTTP in a multi-threaded environment.

## Features
Serve files from a specified directory
Listen on a user-defined port
Handle multiple concurrent requests using Goroutines
Support for basic file operations like downloading files
## Prerequisites
To use the Multithreaded File Server, you need the following:

* Go (version 1.16 or later) installed on your system
## Usage
### Building from Source
1. Clone the repository:

```bash
git clone https://github.com/your-username/multithreadedfileserver.git
```
2. Change to the project directory:
```bash
cd multithreadedfileserver
```
3. Set the serving directory and port as environment variables:

```bash
export DIRECTORY=/srv
export PORT=8000
```
4. Build and run the server:

```bash
go build
./multithreadedfileserver
```
5. Access the server in your web browser:

```js
http://localhost:8000/
```
The server will list the files in the specified directory. Click on a file to download it.
### Using Docker
1. Run the docker command:
```bash
docker run -v /path/to/directory:/srv -p 8000:8000 registry.gitlab.com/teeco-devops/tools/multithreadedfileserver-go:latest
```
2. Open in the browser
```js
http://localhost:8000/
```
## Configuration
The Multithreaded File Server can be configured using environment variables:

* DIRECTORY: The path to the directory containing the files to be served. Default: /srv.
* PORT: The port on which the server listens for incoming connections. Default: 8000.
