package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {
	PORT := GetEnvOrDefault("PORT", "8000")
	DIRECTORY := GetEnvOrDefault("DIRECTORY", "/srv")

	// Register a handler to serve files from the current directory
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Get the requested file path
		filePath := filepath.Join(DIRECTORY, r.URL.Path)
		fileInfo, err := os.Stat(filePath)
		if err != nil {
			// Handle error
			http.NotFound(w, r)
			log.Printf("File not found: %v", filePath)
			return
		}

		// Check if it's a directory
		if fileInfo.IsDir() {
			dir, err := os.Open(filePath)
			if err != nil {
				// Handle the error
				log.Printf("Encountered error: %v\n", err)
				return
			}
			defer dir.Close()

			// Read the directory contents
			fmt.Fprintf(w, `<html>
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <title>Directory listing for /</title>
                        </head>
                        <body>
                        <h1>Directory listing for %s</h1>
                        <hr>
                        <ul>
                        `, filePath)
			children, _ := dir.Readdir(0)

			for _, child := range children {
				if child.Mode().IsDir() {
					fmt.Fprintf(w, `<li><a href="%s/">%s/</a></li>`, child.Name(), child.Name())
				} else {
					fmt.Fprintf(w, `<li><a href="%s">%s</a></li>`, child.Name(), child.Name())

				}
			}
			fmt.Fprintf(w, `</ul>
                        <hr>
                        </body>
                        </html>`)
			log.Printf("Served Directory: %v", filePath)
		} else {

			file, err := os.Open(filePath)
			if err != nil {
				http.NotFound(w, r)
				return
			}
			defer file.Close()

			// Check if the file supports partial content
			acceptRanges := fileInfo.Size() > 0
			fileSize := strconv.FormatInt(fileInfo.Size(), 10)

			// Set the headers for partial content support
			if acceptRanges {
				w.Header().Set("Accept-Ranges", "bytes")
			}
			log.Printf("Serving File: %v", filePath)

			// Handle range requests
			if rangeHeader := r.Header.Get("Range"); acceptRanges && rangeHeader != "" {
				servePartialContent(w, r, file, fileSize)
			} else {
				// Serve the complete file
				http.ServeContent(w, r, fileInfo.Name(), fileInfo.ModTime(), file)
			}
		}
	})

	// Start the server
	log.Printf("Server started on http://localhost:%s", PORT)
	log.Printf("Serving files under %s", DIRECTORY)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", PORT), nil))
}
func GetEnvOrDefault(envVarName, defaultValue string) string {
	envVarValue, exists := os.LookupEnv(envVarName)
	if exists {
		return envVarValue
	}
	return defaultValue
}

// Serve partial content (range requests)
func servePartialContent(w http.ResponseWriter, r *http.Request, file *os.File, fileSize string) {
	fileInfo, _ := file.Stat()
	fileSizeInt := fileInfo.Size()

	rangeHeader := r.Header.Get("Range")
	rangeValue := rangeHeader[len("bytes="):]

	// Parse the range value
	start, end := parseRange(rangeValue, fileSizeInt)

	// Set the response headers for partial content
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Length", strconv.FormatInt(end-start+1, 10))
	w.Header().Set("Content-Range", "bytes "+strconv.FormatInt(start, 10)+"-"+strconv.FormatInt(end, 10)+"/"+fileSize)

	// Set the response status code to 206 Partial Content
	w.WriteHeader(http.StatusPartialContent)

	// Seek to the appropriate position in the file
	file.Seek(start, 0)

	// Create a limited reader to serve only the specified range
	limitReader := &io.LimitedReader{R: file, N: end - start + 1}

	// Serve the limited range
	io.Copy(w, limitReader)
}

// Parse the range header value
func parseRange(rangeValue string, fileSize int64) (start, end int64) {
	parts := strings.Split(rangeValue, "-")
	start, _ = strconv.ParseInt(parts[0], 10, 64)
	if len(parts) > 1 {
		end, _ = strconv.ParseInt(parts[1], 10, 64)
		if end >= fileSize || end == 0 {
			end = fileSize - 1
		}
	} else {
		end = fileSize - 1
	}
	return start, end
}
