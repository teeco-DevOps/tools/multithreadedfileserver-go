FROM golang:1.20-alpine as builder
WORKDIR /go/src/app
COPY . .
RUN CGO_ENABLED=0 go install -ldflags '-extldflags "-static"' -tags timetzdata
FROM scratch
COPY --from=builder /go/bin/multithreadedfileserver-go /multithreadedfileserver
ENTRYPOINT ["/multithreadedfileserver"]